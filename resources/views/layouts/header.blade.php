      <nav class="navbar header-navbar">
         <div class="navbar-wrapper">
            <div class="navbar-logo">
               <a class="mobile-menu" id="mobile-collapse" href="#!">
               <i class="ti-menu"></i>
               </a>
               <a class="mobile-search morphsearch-search" href="#">
               <i class="ti-search"></i>
               </a>
               <a href="index-2.html">
               <img class="img-fluid" src="{{ asset('app_assets/assets/images/logo.png')}}" alt="Theme-Logo" />
               </a>
               <a class="mobile-options">
               <i class="ti-more"></i>
               </a>
            </div>
            <div class="navbar-container container-fluid">
               <div>
                  <ul class="nav-left">
                     <li>
                        <a id="collapse-menu" href="#">
                        <i class="ti-menu"></i>
                        </a>
                     </li>
                     <li>
                        <a class="main-search morphsearch-search" href="#">
                        <i class="ti-search"></i>
                        </a>
                     </li>
                     <li>
                        <a href="#!" onclick="javascript:toggleFullScreen()">
                        <i class="ti-fullscreen"></i>
                        </a>
                     </li>
                     
                  </ul>
                  <ul class="nav-right">
                     <li class="header-notification">
                        <a href="#!">
                        <i class="ti-bell"></i>
                        <span class="badge">5</span>
                        </a>
                        <ul class="show-notification">
                           <li>
                              <h6>Notifications</h6>
                              <label class="label label-danger">New</label>
                           </li>
                           <li>
                              <div class="media">
                                 <img class="d-flex align-self-center" src="{{ asset('app_assets/assets/images/user.png')}}" alt="Generic placeholder image">
                                 <div class="media-body">
                                    <h5 class="notification-user"></h5>
                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                    <span class="notification-time">30 minutes ago</span>
                                 </div>
                              </div>
                           </li>
                           
                        </ul>
                     </li>
                     
                     <li class="user-profile header-notification">
                        <a href="#!">
                        <img src="{{ asset('app_assets/assets/images/user.png')}}" alt="User-Profile-Image">
                        <span> @if(Session()->has('Username'))
                                   {{ Session()->get('Username')}}
                              @endif

                        </span>
                        <i class="ti-angle-down"></i>
                        </a>
                        <ul class="show-notification profile-notification">
                           <li>
                              <a href="{{ url('settings')}}">
                              <i class="ti-settings"></i> Settings
                              </a>
                           </li>
                           <li>
                              <a href="user-profile.html">
                              <i class="ti-user"></i> Profile
                              </a>
                           </li>
                           <!-- <li>
                              <a href="email-inbox.html">
                              <i class="ti-email"></i> My Messages
                              </a>
                           </li> 
                           <li>
                              <a href="auth-lock-screen.html">
                              <i class="ti-lock"></i> Lock Screen
                              </a>
                           </li> -->
                           <li>
                              <a href="{{ url('logout')}}">
                              <i class="ti-layout-sidebar-left"></i> Logout
                              </a>
                           </li>
                        </ul>
                     </li>
                  </ul>
                  <div id="morphsearch" class="morphsearch">
                     <form class="morphsearch-form">
                        <input class="morphsearch-input" type="search" placeholder="Search..." />
                        <button class="morphsearch-submit" type="submit">Search</button>
                     </form>
                     <div class="morphsearch-content">
                        <div class="dummy-column">
                           <h2>People</h2>
                           <a class="dummy-media-object" href="#!">
                              <img class="round" src="http://0.gravatar.com/avatar/81b58502541f9445253f30497e53c280?s=50&amp;d=identicon&amp;r=G" alt="Sara Soueidan" />
                              <h3>Sara Soueidan</h3>
                           </a>
                           <a class="dummy-media-object" href="#!">
                              <img class="round" src="http://1.gravatar.com/avatar/9bc7250110c667cd35c0826059b81b75?s=50&amp;d=identicon&amp;r=G" alt="Shaun Dona" />
                              <h3>Shaun Dona</h3>
                           </a>
                        </div>
                        
                     </div>
                     <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
                  </div>
               </div>
            </div>
         </div>
      </nav>

      

   