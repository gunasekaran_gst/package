<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Package</title>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="Phoenixcoded">
      <meta name="keywords" content="flat ui, admin , Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="Phoenixcoded">
      <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/icon/themify-icons/themify-icons.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/icon/icofont/css/icofont.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/pages/flag-icon/flag-icon.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/pages/menu-search/css/component.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/pages/dashboard/horizontal-timeline/css/style.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/css/style.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/css/color/color-1.css') }}" id="color" />
      <!--css-->

      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/assets/pages/data-table/css/buttons.dataTables.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('app_assets/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
      <!--  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"> -->
   </head>
   <body class="fix-menu dark-layout">
      <div class="theme-loader">
         <div class="ball-scale">
            <div></div>
         </div>
      </div>
       <!-- Header -->
  @include('layouts.header')
    <!-- Sidebar -->
  @include('layouts.sidebar')


    <!-- Your Page Content Here -->
            @yield('content')



      <script type="text/javascript" src="{{ asset('app_assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/tether/dist/js/tether.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/modernizr/modernizr.js') }}"></script>
     <script type="text/javascript" src="{{ asset('app_assets/bower_components/modernizr/feature-detects/css-scrollbars.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/classie/classie.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/raphael/raphael.min.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/morris.js/morris.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/assets/pages/todo/todo.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/assets/pages/dashboard/horizontal-timeline/js/main.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/i18next/i18next.min.js') }}"></script>
      <!--data table-->


      <script src="{{ asset('app_assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
      <script src="{{ asset('app_assets/assets/pages/data-table/js/jszip.min.js') }}"></script>
      <script src="{{ asset('app_assets/assets/pages/data-table/js/pdfmake.min.js') }}"></script>
      <script src="{{ asset('app_assets/assets/pages/data-table/js/vfs_fonts.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
      <script src="{{ asset('app_assets/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
      <script src="{{ asset('app_assets/assets/pages/data-table/js/data-table-custom.js') }}"></script>
      <!--data table-->

      <script type="text/javascript" src="{{ asset('app_assets/bower_components/i18next-xhr-backend/i18nextXHRBackend.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/i18next-browser-languagedetector/i18nextBrowserLanguageDetector.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/bower_components/jquery-i18next/jquery-i18next.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/assets/pages/dashboard/project-dashboard.js') }}"></script>
   
      <script type="text/javascript" src="{{ asset('app_assets/assets/js/script.js') }}"></script>
      <!--validation-->
      <script type="text/javascript" src="{{ asset('app_assets/assets/js/custom_validate.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/assets/pages/form-validation/validate.js') }}"></script>
      <script type="text/javascript" src="{{ asset('app_assets/assets/pages/form-validation/form-validation.js') }}"></script>
      <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
 -->

      
    
   </body>
</html>