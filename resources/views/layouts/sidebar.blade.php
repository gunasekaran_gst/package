      <div class="main-menu">
         <div class="main-menu-header">
            <img class="img-40" src="{{ asset('app_assets/assets/images/user.png') }}" alt="User-Profile-Image">
            <div class="user-details">
               <span>John Doe</span>
               <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>
            </div>
         </div>
         <div class="main-menu-content">
            <ul class="main-navigation">
               <li class="more-details">
                  <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                  <a href="{{ url('admin/settings')}}"><i class="ti-settings"></i>Settings</a>
                  <a href="{{ url('logout')}}"><i class="ti-layout-sidebar-left"></i>Logout</a>
               </li>
              
               
                <li class="nav-item single-item">
                  <a href="{{ url('dashboard')}}">
                  <i class="ti-home"></i>
                  <span data-i18n="nav.dash.main">Dashboard</span>
                  </a>
                 
               </li>
               
               
               <li class="nav-item">
                  <a href="#!">
                  <i class="ti-direction-alt"></i>
                  <span data-i18n="nav.menu-levels.main">Master</span>
                  </a>
                  <ul class="tree-1">
                     <li><a href="admin/gsm" data-i18n="nav.menu-levels.menu-level-21">GSM Master</a></li>
                     <li><a href="admin/bf" data-i18n="nav.menu-levels.menu-level-21">BF Master</a></li>
                     <li class="nav-sub-item">
                        <a href="#" data-i18n="nav.menu-levels.menu-level-22.main">Material Master</a>
                        <ul class="tree-2">
                           <li><a href="admin/materialcategory" data-i18n="nav.menu-levels.menu-level-22.menu-level-31">Material Category</a></li>
                           <li><a href="admin/material" data-i18n="nav.menu-levels.menu-level-22.menu-level-31">Material </a></li>
                           
                        </ul>
                     </li>
                     <li><a href="{{ url('admin/customer')}}" data-i18n="nav.menu-levels.menu-level-23">Customer Master</a></li>
                     <li><a href="{{ url('admin/supplier')}}" data-i18n="nav.menu-levels.menu-level-23">Supplier Master</a></li>
                     <li><a href="user" data-i18n="nav.menu-levels.menu-level-23">User Master</a></li>
                  </ul>
               </li>

               <li class="nav-item">
                  <a href="#!">
                  <i class="ti-crown"></i>
                  <span data-i18n="nav.menu-levels.main">Purchase</span>
                  </a>
                  <ul class="tree-1">
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Requirement</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">PUrchase Order [ PO ]</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-23">Material Inward</a></li>
                    
                  </ul>
               </li>
               <li class="nav-item">
                  <a href="#!">
                  <i class="ti-layers-alt"></i>
                  <span data-i18n="nav.menu-levels.main">Marketing</span>
                  </a>
                  <ul class="tree-1">
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Orders</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">JOb Works</a></li>
                     
                    
                  </ul>
               </li>
               <li class="nav-item">
                  <a href="#!">
                  <i class="ti-star"></i>
                  <span data-i18n="nav.menu-levels.main">Production</span>
                  </a>
                  <ul class="tree-1">
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Planning</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Order</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Production Process</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Stock</a></li>
                     
                    
                  </ul>
               </li>

               <li class="nav-item">
                  <a href="#!">
                  <i class="ti-layers"></i>
                  <span data-i18n="nav.menu-levels.main">Dispatch</span>
                  </a>
                  <ul class="tree-1">
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Sale Order</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Customer [ PO ]</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Sale Invoice</a></li>
                     
                     
                    
                  </ul>
               </li>
               

               <li class="nav-item">
                  <a href="#!">
                  <i class="ti-receipt"></i>
                  <span data-i18n="nav.menu-levels.main">Report</span>
                  </a>
                  <ul class="tree-1">
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Customer Report</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Supplier Report</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">User Report</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Purchase Report</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Stock Report</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Sale Report</a></li>
                     <li><a href="#!" data-i18n="nav.menu-levels.menu-level-21">Customer ( PO ) Report</a></li>
                   
                     
                     
                    
                  </ul>
               </li>
              
            </ul>
         </div>
      </div>
      <!-- <div id="sidebar" class="users p-chat-user showChat">
         <div class="had-container">
            <div class="card card_main p-fixed users-main">
               <div class="user-box">
                  <div class="card-block">
                     <div class="right-icon-control">
                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                        <div class="form-icon">
                           <i class="icofont icofont-search"></i>
                        </div>
                     </div>
                  </div>
                  <div class="main-friend-list">
                     <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Josephin Doe</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/task/task-u1.jpg" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Lary Doe</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Alice</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/task/task-u2.jpg" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Alia</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/task/task-u3.jpg" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Suzen</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="6" data-status="offline" data-username="Michael Scofield" data-toggle="tooltip" data-placement="left" title="Michael Scofield">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-3.png" alt="Generic placeholder image">
                           <div class="live-status bg-danger"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Michael Scofield</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="7" data-status="online" data-username="Irina Shayk" data-toggle="tooltip" data-placement="left" title="Irina Shayk">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-4.png" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Irina Shayk</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="8" data-status="offline" data-username="Sara Tancredi" data-toggle="tooltip" data-placement="left" title="Sara Tancredi">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-5.png" alt="Generic placeholder image">
                           <div class="live-status bg-danger"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Sara Tancredi</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="9" data-status="online" data-username="Samon" data-toggle="tooltip" data-placement="left" title="Samon">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Samon</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="10" data-status="online" data-username="Daizy Mendize" data-toggle="tooltip" data-placement="left" title="Daizy Mendize">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/task/task-u3.jpg" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Daizy Mendize</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="11" data-status="offline" data-username="Loren Scofield" data-toggle="tooltip" data-placement="left" title="Loren Scofield">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-3.png" alt="Generic placeholder image">
                           <div class="live-status bg-danger"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Loren Scofield</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="12" data-status="online" data-username="Shayk" data-toggle="tooltip" data-placement="left" title="Shayk">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-4.png" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Shayk</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="13" data-status="offline" data-username="Sara" data-toggle="tooltip" data-placement="left" title="Sara">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/task/task-u3.jpg" alt="Generic placeholder image">
                           <div class="live-status bg-danger"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Sara</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="14" data-status="online" data-username="Doe" data-toggle="tooltip" data-placement="left" title="Doe">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Doe</div>
                        </div>
                     </div>
                     <div class="media userlist-box" data-id="15" data-status="online" data-username="Lary" data-toggle="tooltip" data-placement="left" title="Lary">
                        <a class="media-left" href="#!">
                           <img class="media-object img-circle" src="assets/images/task/task-u1.jpg" alt="Generic placeholder image">
                           <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                           <div class="f-13 chat-header">Lary</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
 -->
      <!-- <div class="showChat_inner">
         <div class="media chat-inner-header">
            <a class="back_chatBox">
            <i class="icofont icofont-rounded-left"></i> Josephin Doe
            </a>
         </div>
         <div class="media chat-messages">
            <a class="media-left photo-table" href="#!">
            <img class="media-object img-circle m-t-5" src="assets/images/avatar-1.png" alt="Generic placeholder image">
            </a>
            <div class="media-body chat-menu-content">
               <div class="">
                  <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                  <p class="chat-time">8:20 a.m.</p>
               </div>
            </div>
         </div>
         <div class="media chat-messages">
            <div class="media-body chat-menu-reply">
               <div class="">
                  <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                  <p class="chat-time">8:20 a.m.</p>
               </div>
            </div>
            <div class="media-right photo-table">
               <a href="#!">
               <img class="media-object img-circle m-t-5" src="assets/images/avatar-2.png" alt="Generic placeholder image">
               </a>
            </div>
         </div>
         <div class="chat-reply-box p-b-20">
            <div class="right-icon-control">
               <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
               <div class="form-icon">
                  <i class="icofont icofont-paper-plane"></i>
               </div>
            </div>
         </div>
      </div> -->