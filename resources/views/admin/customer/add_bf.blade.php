@extends('layouts.master')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header">
                <div class="page-header-title">
                    <h4>Manage BF </h4>
                    <span></span>
                </div>
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index-2.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">
                                @if(Session()->has('UserId'))
                                    <span style="color: red;">{{ Session()->get('UserId')}}</span>
                                @endif</a></li>
                        <li class="breadcrumb-item"><a href="#!">Home</a></li>

                        <li class="breadcrumb-item"><a href="#!">Add BF</a></li>
                    </ul>
                </div>
            </div>
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs  tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link <?php if(isset($add)){ echo 'active'; }?>" data-toggle="tab" href="#add-content" role="tab">Add BF</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php if(isset($list)){ echo 'active'; }?>" data-toggle="tab" href="#list-content" role="tab">BF List</a>
                            </li>
                            <?php if(isset($edit)){ ?>
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#edit-content" role="tab">Edit BF</a>
                            </li>
                            <?php } ?>

                        </ul>
                        <div class="tab-content tabs card-block">
                            <div class="tab-pane <?php if(isset($add)){ echo 'active'; }?>" id="add-content" role="tabpanel">

                                <div class="card" id="tab-content">
                                    <div class="card-header">
                                        <h5>Add BF</h5>
                                        <span></span>
                                        <div class="card-header-right">
                                            <i class="icofont icofont-rounded-down"></i>
                                            <i class="icofont icofont-refresh"></i>
                                            <i class="icofont icofont-close-circled"></i>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <h4 class="sub-title">Basic Requirements For BF</h4>

                                        <center>
                                            @if(session()->has('message'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('message') }}
                                                </div>
                                            @endif

                                        </center>

                                        <form action="{{ url('admin/add_bf')}}" method="post" id="form" class="customer-validation">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">GSMName</label>
                                                <div class="col-sm-6">

                                                    <input type="text" name="BFName" id="BFName"  class="BFName form-control form-control-round form-txt-default" placeholder="BFName" value="{{ old('BFName') }}">
                                                    <span style="color:red;" id="err_code"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Status</label>
                                                <div class="col-sm-6">

                                                    <input type="text" name="Status" id="Status"  class="form-control form-control-round form-txt-default" placeholder="Status" value="{{ old('Status') }}">
                                                    <span style="color:red;" id="err_name"></span>

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2"></label>
                                                <div class="col-sm-2">
                                                    <button type="submit" value="submit" id="submit" class="btn btn-primary btn-round">Save</button>
                                                    <button type="reset" name="reset" class="btn btn-danger btn-round">Clear</button>
                                                </div>
                                                <div class="col-sm-2">

                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane <?php if(isset($list)){ echo 'active'; }?>" id="list-content" role="tabpanel">

                                <div class="card" id="tab-content2">
                                    <div class="card-header">
                                        <h5>GSM List</h5>

                                        <span></span>
                                        <div class="card-header-right">
                                            <i class="icofont icofont-rounded-down"></i>
                                            <i class="icofont icofont-refresh"></i>
                                            <i class="icofont icofont-close-circled"></i>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                <tr>
                                                    <td>Id</td>
                                                    <td>BFName </td>
                                                    <td>Status </td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $count = 1; if(isset($user_list) && !empty($user_list)){ ?>
                                                @foreach ($user_list as $users)
                                                    <tr>
                                                        <td>{{ $users->UniqueId }}</td>
                                                        <td>{{ $users->BFName }}</td>
                                                        <td>{{ $users->Status }}</td>

                                                        <td><a href="edit_bf/{{ $users->UniqueId }}"><button class="btn btn-primary btn-round">Edit</button></a>

                                                            <a href="delete_bf/{{ $users->UniqueId }}"><button OnClick="return confirm('Are You Sure Want to Delete?')" class="btn btn-danger btn-round">Delete</button></a>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                                <?php } ?>
                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <?php if(isset($edit)){ ?>
                            <div class="tab-pane active" id="edit-content" role="tabpanel">

                                <div class="card" id="tab-content">
                                    <div class="card-header">
                                        <h5>Edit BF</h5>
                                        <span></span>
                                        <div class="card-header-right">
                                            <i class="icofont icofont-rounded-down"></i>
                                            <i class="icofont icofont-refresh"></i>
                                            <i class="icofont icofont-close-circled"></i>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <h4 class="sub-title">Basic Requirements For BF</h4>

                                        <center>
                                            @if(session()->has('message'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('message') }}
                                                </div>
                                            @endif

                                        </center>

                                        <form action="{{ url('/admin/edit_bf/'.$user[0]->UniqueId) }}" method="post" id="form" class="customer-validation">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">BFName</label>
                                                <div class="col-sm-6">

                                                    <input type="text" name="BFName" id="BFName"  class="GSMName form-control form-control-round form-txt-default" placeholder="GSMName" value="<?php echo $user[0]->BFName; ?>">
                                                    <span style="color:red;" id="err_code"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Status</label>
                                                <div class="col-sm-6">

                                                    <input type="text" name="Status" id="Status"  class="form-control form-control-round form-txt-default" placeholder="Status" value="<?php echo $user[0]->Status; ?>">
                                                    <span style="color:red;" id="err_name"></span>

                                                </div>
                                            </div>

                                            <input type="hidden" name="UniqueId" id="UniqueId" value="<?php echo $user[0]->UniqueId; ?>" />

                                            <div class="form-group row">
                                                <label class="col-sm-2"></label>
                                                <div class="col-sm-2">
                                                    <button type="submit" value="update" id="submit" class="btn btn-primary btn-round">Save</button>
                                                    <a href="{{ url('/admin/bf') }}" type="reset" name="reset" class="btn btn-danger btn-round">Cancel</a>
                                                </div>

                                            </div>
                                        </form>


                                    </div>
                                </div>

                            </div>
                            <?php } ?>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
