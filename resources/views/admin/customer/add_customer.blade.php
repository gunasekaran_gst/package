@extends('layouts.master')

@section('content')

   <div class="main-body">
      <div class="page-wrapper">
         <div class="page-header">
            <div class="page-header-title">
               <h4>Manage Customer </h4>
               <span></span>
            </div>
            <div class="page-header-breadcrumb">
               <ul class="breadcrumb-title">
                  <li class="breadcrumb-item">
                     <a href="index-2.html">
                        <i class="icofont icofont-home"></i>
                     </a>
                  </li>
                  <li class="breadcrumb-item"><a href="#!">
                                 @if(Session()->has('BranchCode'))
                                   <span style="color: red;">{{ Session()->get('BranchCode')}}</span>
                                 @endif</a></li>
                  <li class="breadcrumb-item"><a href="#!">Home</a></li>

                  <li class="breadcrumb-item"><a href="#!">Add Customer</a></li>
               </ul>
            </div>
         </div>
         <div class="page-body">
            <div class="row">
               <div class="col-sm-12">
                  <ul class="nav nav-tabs  tabs" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link <?php if(isset($add)){ echo 'active'; }?>" data-toggle="tab" href="#add-content" role="tab">Add Customer</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link <?php if(isset($list)){ echo 'active'; }?>" data-toggle="tab" href="#list-content" role="tab">Customer List</a>
                     </li>
                       <?php if(isset($edit)){ ?>
                     <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#edit-content" role="tab">Edit Customer</a>
                     </li>
                      <?php } ?> 

                  </ul>
                  <div class="tab-content tabs card-block">
                     <div class="tab-pane <?php if(isset($add)){ echo 'active'; }?>" id="add-content" role="tabpanel">

                        <div class="card" id="tab-content">
                           <div class="card-header">
                              <h5>Add Customer</h5>
                              <span></span>
                              <div class="card-header-right">
                                 <i class="icofont icofont-rounded-down"></i>
                                 <i class="icofont icofont-refresh"></i>
                                 <i class="icofont icofont-close-circled"></i>
                              </div>
                           </div>
                           <div class="card-block">
                              <h4 class="sub-title">Basic Requirements For Customer</h4>

                              <center>
                                 @if(session()->has('message'))
                                    <div class="alert alert-success">
                                       {{ session()->get('message') }}
                                    </div>
                                 @endif
                                 
                              </center>
                              
                              <form action="{{ url('admin/add_customer')}}" method="post" id="form" class="customer-validation">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Customer Code</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="CustomerCode" id="CustomerCode"  class="CustomerCode form-control form-control-round form-txt-default" placeholder="Customer Code" value="{{ old('Customer Code') }}">
                                        <span style="color:red;" id="err_code"></span>
                                          
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">CustomerName</label>
                                    <div class="col-sm-6">
                                       
                                          <input type="text" name="CustomerName" id="CustomerName"  class="form-control form-control-round form-txt-default" placeholder="CustomerName" value="{{ old('CustomerName') }}">
                                          <span style="color:red;" id="err_name"></span>

                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">PinCode</label>
                                    <div class="col-sm-6">
                                       
                                          <input type="text" name="PinCode" id="PinCode"  class="form-control form-control-round form-txt-default" placeholder="PinCode" value="{{ old('PinCode') }}">
                                           <span style="color:red;" id="err_pin"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Mobile No</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="MobileNo" id="MobileNo"  class="form-control form-control-round form-txt-default" placeholder="MobileNo" value="{{ old('MobileNo') }}">
                                           <span style="color:red;" id="err_mobile"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">EmailId</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="EmailId" id="EmailId"  class="form-control form-control-round form-txt-default" placeholder="EmailId" value="{{ old('EmailId') }}">
                                          <span style="color:red;" id="err_email"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-6">
                                       <div class="checkbox-fade fade-in-default">
                                           <label>
                                              <input type="checkbox" value="Y" checked="true" name="Status" id="Status">
                                           <span class="cr">
                                           <i class="cr-icon icofont icofont-ui-check txt-default"></i>
                                           </span> <span>Default</span>
                                           </label>
                                       </div>
                                          
                                           <!-- <span style="color:red;" id="err_email"></span> -->

                                    </div>
                                 </div>


                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ContactNo</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="ContactNo" id="ContactNo"  class="form-control form-control-round form-txt-default" placeholder="ContactNo" value="{{ old('ContactNo') }}">
                                           <span style="color:red;" id="err_cno"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ContactEmail Id</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="ContactEmailId" id="ContactEmailId"  class="form-control form-control-round form-txt-default" placeholder="ContactEmailId" value="{{ old('ContactEmailId') }}">
                                           <span style="color:red;" id="err_cemail"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Contact PersonName</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="ContactPersonName" id="ContactPersonName"  class="form-control form-control-round form-txt-default" placeholder="ContactPersonName" value="{{ old('ContactPersonName') }}">
                                          <span style="color:red;" id="err_cpname"></span>
                                      </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">BranchCode</label>
                                    <div class="col-sm-6">
                                       
                                          <input type="text" name="BranchCode" id="BranchCode"  class="form-control form-control-round form-txt-default" placeholder="BranchCode" value="{{ old('BranchCode') }}">
                                           <span style="color:red;" id="err_branchcode"></span>
                                       </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-6">
                                       
                                          <textarea rows="5" cols="5" name="Address" id="Address"  class="form-control form-txt-default" placeholder="Address" value="{{ old('Address') }}"></textarea>
                                           <span style="color:red;" id="err_address"></span>
                                    </div>
                                 </div>




                                 <div class="form-group row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-2">
                                       <button type="submit" value="submit" id="submit" class="btn btn-primary btn-round">Save</button>
                                       <button type="reset" name="reset" class="btn btn-danger btn-round">Clear</button>
                                    </div>
                                    <div class="col-sm-2">
                                       
                                    </div>
                                 </div>
                              </form>


                           </div>
                        </div>

                     </div>
                     <div class="tab-pane <?php if(isset($list)){ echo 'active'; }?>" id="list-content" role="tabpanel">

                        <div class="card" id="tab-content2">
                           <div class="card-header">
                              <h5>Customer List</h5>

                              <span></span>
                              <div class="card-header-right">
                                 <i class="icofont icofont-rounded-down"></i>
                                 <i class="icofont icofont-refresh"></i>
                                 <i class="icofont icofont-close-circled"></i>
                              </div>
                           </div>
                           <div class="card-block">
                              <div class="dt-responsive table-responsive">
                                 <table id="simpletable" class="table table-striped table-bordered nowrap">
                                     <thead>
                                       <tr>
                                       <td>Id</td>
                                       <td>C-Code </td>
                                       <td>CustomerName </td>
                                       <td>Address </td>
                                       <td>PinCode </td>
                                       <td>MobileNo </td>
                                       <td>EmailId </td>
                                       <td>Status</td>
                                     
                                       <td>Action</td>   
                                      </tr>
                                    </thead>
                                 <tbody>
                                     <?php $count = 1; if(isset($user_list) && !empty($user_list)){ ?>
                                    @foreach ($user_list as $users)
                                     <tr>
                                       <td>{{ $users->UniqueId }}</td>
                                       <td>{{ $users->CustomerCode }}</td>
                                       <td>{{ $users->CustomerName }}</td>
                                       <td>{{ $users->Address }}</td>
                                       <td>{{ $users->PinCode }}</td>
                                       <td>{{ $users->MobileNo }}</td>
                                       <td>{{ $users->EmailId }}</td>
                                       <td>{{ $users->Status }}</td>
                                      
                                       <td><a href="edit_customer/{{ $users->UniqueId }}"><button class="btn btn-primary btn-round">Edit</button></a>

                                          <a href="delete_customer/{{ $users->UniqueId }}"><button OnClick="return confirm('Are You Sure Want to Delete?')" class="btn btn-danger btn-round">Delete</button></a>
                                       </td>
                                    </tr>
                                   
                                     @endforeach
                                     <?php } ?>
                                 </tbody>
                                 <tfoot>
         
                                 </tfoot>
                                </table>
                              </div>
                           </div>
                        </div>

                     </div>
                     <?php if(isset($edit)){ ?>
                     <div class="tab-pane active" id="edit-content" role="tabpanel">

                        <div class="card" id="tab-content">
                           <div class="card-header">
                              <h5>Edit Customer</h5>
                              <span></span>
                              <div class="card-header-right">
                                 <i class="icofont icofont-rounded-down"></i>
                                 <i class="icofont icofont-refresh"></i>
                                 <i class="icofont icofont-close-circled"></i>
                              </div>
                           </div>
                           <div class="card-block">
                              <h4 class="sub-title">Basic Requirements For Customer</h4>

                              <center>
                                 @if(session()->has('message'))
                                    <div class="alert alert-success">
                                       {{ session()->get('message') }}
                                    </div>
                                 @endif
                                 
                              </center>
                              
                              <form action="{{ url('/admin/edit_customer/'.$user[0]->UniqueId) }}" method="post" id="form" class="customer-validation">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Customer Code</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="CustomerCode" id="CustomerCode"  class="CustomerCode form-control form-control-round form-txt-default" placeholder="Customer Code" value="<?php echo $user[0]->CustomerCode; ?>">
                                          <span style="color:red;" id="err_code"></span>
                                          
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">CustomerName</label>
                                    <div class="col-sm-6">
                                       
                                          <input type="text" name="CustomerName" id="CustomerName"  class="form-control form-control-round form-txt-default" placeholder="CustomerName" value="<?php echo $user[0]->CustomerName; ?>">
                                          <span style="color:red;" id="err_name"></span>

                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">PinCode</label>
                                    <div class="col-sm-6">
                                       
                                          <input type="text" name="PinCode" id="PinCode"  class="form-control form-control-round form-txt-default" placeholder="PinCode" value="<?php echo $user[0]->PinCode; ?>">
                                           <span style="color:red;" id="err_pin"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Mobile No</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="MobileNo" id="MobileNo"  class="form-control form-control-round form-txt-default" placeholder="MobileNo" value="<?php echo $user[0]->MobileNo; ?>">
                                           <span style="color:red;" id="err_mobile"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">EmailId</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="EmailId" id="EmailId"  class="form-control form-control-round form-txt-default" placeholder="EmailId" value="<?php echo $user[0]->EmailId; ?>">
                                          <span style="color:red;" id="err_email"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-6">
                                       <div class="checkbox-fade fade-in-default">
                                           <label>
                                              <input type="checkbox" value="<?php echo $user[0]->Status; ?>" checked="true" name="Status" id="Status">
                                           <span class="cr">
                                           <i class="cr-icon icofont icofont-ui-check txt-default"></i>
                                           </span> <span>Default</span>
                                           </label>
                                       </div>
                                          
                                           <!-- <span style="color:red;" id="err_email"></span> -->

                                    </div>
                                 </div>


                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ContactNo</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="ContactNo" id="ContactNo"  class="form-control form-control-round form-txt-default" placeholder="ContactNo" value="<?php echo $user[0]->ContactNo; ?>">
                                           <span style="color:red;" id="err_cno"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ContactEmail Id</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="ContactEmailId" id="ContactEmailId"  class="form-control form-control-round form-txt-default" placeholder="ContactEmailId" value="<?php echo $user[0]->ContactEmailId; ?>">
                                           <span style="color:red;" id="err_cemail"></span>
                                    </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Contact PersonName</label>
                                    <div class="col-sm-6">
                                      
                                          <input type="text" name="ContactPersonName" id="ContactPersonName"  class="form-control form-control-round form-txt-default" placeholder="ContactPersonName" value="<?php echo $user[0]->ContactPersonName; ?>">
                                          <span style="color:red;" id="err_cpname"></span>
                                      </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">BranchCode</label>
                                    <div class="col-sm-6">
                                       
                                          <input type="text" name="BranchCode" id="BranchCode"  class="form-control form-control-round form-txt-default" placeholder="BranchCode" value="<?php echo $user[0]->BranchCode; ?>">
                                           <span style="color:red;" id="err_branchcode"></span>
                                       </div>
                                 </div>

                                 <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-6">
                                       
                                          <textarea rows="5" cols="5" name="Address" id="Address"  class="form-control form-txt-default" placeholder="Address" value="<?php echo $user[0]->Address; ?>"><?php echo $user[0]->Address; ?></textarea>
                                           <span style="color:red;" id="err_address"></span>
                                    </div>
                                 </div>


                                  <input type="hidden" name="UniqueId" id="UniqueId" value="<?php echo $user[0]->UniqueId; ?>" />

                                 <div class="form-group row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-2">
                                       <button type="submit" value="update" id="submit" class="btn btn-primary btn-round">Save</button>
                                        <a href="{{ url('/admin/customer') }}" type="reset" name="reset" class="btn btn-danger btn-round">Cancel</a>
                                    </div>
                                   
                                 </div>
                              </form>


                           </div>
                        </div>

                     </div>
                     <?php } ?>

                  </div>


               </div>
            </div>
         </div>
      </div>
   </div>


@endsection
