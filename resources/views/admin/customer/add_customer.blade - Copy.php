@extends('layouts.master')

@section('content')
      
  
      <div class="main-body">
         <div class="page-wrapper">
            <div class="page-header">
               <div class="page-header-title">
                  <h4>Manage Customer </h4>
                  <span></span>
               </div>
               <div class="page-header-breadcrumb">
                  <ul class="breadcrumb-title">
                     <li class="breadcrumb-item">
                        <a href="index-2.html">
                        <i class="icofont icofont-home"></i>
                        </a>
                     </li>
                     <li class="breadcrumb-item"><a href="#!">Home</a></li>
                     <li class="breadcrumb-item"><a href="#!">Add Customer</a></li>
                  </ul>
               </div>
            </div>
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <ul class="nav nav-tabs  tabs" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" data-toggle="tab" href="#add-content" role="tab">Add Customer</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#list-content" role="tab">Customer List</a>
                        </li>
                                    
                     </ul>
                     <div class="tab-content tabs card-block">
                        <div class="tab-pane active" id="add-content" role="tabpanel">

                        <div class="card" id="tab-content">
                        <div class="card-header">
                           <h5>Add Customer</h5>
                           <span></span>
                           <div class="card-header-right">
                              <i class="icofont icofont-rounded-down"></i>
                              <i class="icofont icofont-refresh"></i>
                              <i class="icofont icofont-close-circled"></i>
                           </div>
                        </div>
                        <div class="card-block">
                           <h4 class="sub-title">Basic Requirements For Customer</h4>
                           <form name="add_customer" id="add_customer" method="POST" action="{{ url('/admin/add_customer') }}" novalidate="novalidate">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                               <!--Sabari Validation-->
                               <!-- @if ($errors->any())
                                 <div class="alert alert-danger">
                                      <ul>
                                      @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                      @endforeach
                                      </ul>
                                 </div>
                               @endif  -->
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Customer Name</label>
                                 <div class="col-sm-6">
                                    <input type="text" name="name" id="name" class="form-control form-control-round form-txt-default" placeholder="Customer Name">
                                 </div>
                                  <center style="color:red;">
                                  @if ($errors->has('name'))
                                      <div class="error">{{ $errors->first('name') }}</div>
                                  @endif
                                  </center>
                              </div>

                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Email Id</label>
                                 <div class="col-sm-6">
                                    <input type="email" name="email" id="email" class="form-control form-control-round form-txt-default" placeholder="Email ID">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Phone No</label>
                                 <div class="col-sm-6">
                                    <input type="text" name="phone" id="phone" class="form-control form-control-round form-txt-default" placeholder="Phone No">
                                 </div>
                              </div>
                             
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Gender</label>
                                 <div class="col-sm-6">
                                    <select name="gender" name="gender" class="form-control form-control-round form-txt-default">
                                       <option value="">Select One Value Only</option>
                                       <option value="Male">Male</option>
                                       <option value="Female">Female</option>
                                       </select>
                                 </div>
                              </div>
                              
                             

                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Address</label>
                                 <div class="col-sm-6">
                                    <textarea rows="5" cols="5" name="address" id="address" class="form-control form-txt-default" placeholder="Address"></textarea>
                                 </div>
                              </div>

                               <div class="form-group row">
                                 <label class="col-sm-2"></label>
                                 <div class="col-sm-2">
                                    <button type="submit" name="submit" class="btn btn-primary m-b-0">Submit</button>
                                 </div>
                                  <div class="col-sm-2">
                                    <button type="reset" name="reset" class="cancel btn btn-primary m-b-0">Cancel</button>
                                 </div>
                              </div>
                           </form>
                           
                           
                        </div>
                        </div>

                        </div>
                        <div class="tab-pane" id="list-content" role="tabpanel">

                        <div class="card" id="tab-content2">
                        <div class="card-header">
                           <h5><button class="add btn btn-inverse btn-round">ADD Customer</button></h5>

                           <span></span>
                           <div class="card-header-right">
                              <i class="icofont icofont-rounded-down"></i>
                              <i class="icofont icofont-refresh"></i>
                              <i class="icofont icofont-close-circled"></i>
                           </div>
                        </div>
                        <div class="card-block">
                     <div class="table-responsive dt-responsive">
                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>Position</th>
                                 <th>Office</th>
                                 <th>Age</th>
                                 <th>Start date</th>
                                 <th>Salary</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>Tiger Nixon</td>
                                 <td>System Architect</td>
                                 <td>Edinburgh</td>
                                 <td>61</td>
                                 <td>2011/04/25</td>
                                 <td>$320,800</td>
                              </tr>
                              
                              <tr>
                                 <td>Donna Snider</td>
                                 <td>Customer Support</td>
                                 <td>New York</td>
                                 <td>27</td>
                                 <td>2011/01/25</td>
                                 <td>$112,000</td>
                              </tr>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th>Name</th>
                                 <th>Position</th>
                                 <th>Office</th>
                                 <th>Age</th>
                                 <th>Start date</th>
                                 <th>Salary</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
                     </div>

                        </div>
                                    
                     </div>
                     
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function() {
            //alert("ok");
             $(".add").click(function () {
             $( "#tab-content" ).show(300);
             $( "#tab-content2" ).hide(300);
           });  
              $(".cancel").click(function () {
             $( "#tab-content" ).hide(300);
             $( "#tab-content2" ).show(300);
            }); 
       });
      </script> -->
      
@endsection      
      