@extends('layouts.master')
@section('content')
 <div class="main-body">
         <div class="page-wrapper">
            <div class="page-header">
               <div class="page-header-title">
                  <h4>Admin Settings</h4>
                 <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span> -->
                 <!--Message Display Here --> 
                     @if(Session::has('flash_message_error'))
                     
                     <div class="alert alert-danger alert-block">
                          <button type="button" class="close" data-dismiss="alert">×</button>  
                          <strong>{!! session('flash_message_error') !!}</strong>
                     </div>
                     @endif
                     @if(Session::has('flash_message_success'))
                     
                     <div class="alert alert-success alert-block">
                          <button type="button" class="close" data-dismiss="alert">×</button>  
                          <strong>{!! session('flash_message_success') !!}</strong>
                     </div>
                     @endif
               </div>
               <div class="page-header-breadcrumb">
                  <ul class="breadcrumb-title">
                     <li class="breadcrumb-item">
                        <a href="{{ url('admin/dashboard')}}">
                        <i class="icofont icofont-home"></i>
                        </a>
                     </li>
                     <li class="breadcrumb-item"><a href="#!">Settings</a></li>
                     
                  </ul>
               </div>
            </div>
           <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header">
                           <h5>Update Password</h5>
                           <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                           <div class="card-header-right">
                              <i class="icofont icofont-rounded-down"></i>
                              <i class="icofont icofont-refresh"></i>
                              <i class="icofont icofont-close-circled"></i>
                           </div>
                        </div>
                        <div class="card-block">
                           <form id="" method="post" action="{{ url('/admin/updatepassword')}}" novalidate="novalidate">
                              <div class="form-group row">
                                 {{ csrf_field() }}
                                 <label class="col-sm-2 col-form-label">Current Password</label>
                                 <div class="col-sm-10">
                                    <input type="text" class="form-control" name="current_password" id="current_password" placeholder="Current Password">
                                    <span class="messages" id="check_pass"></span>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">New Password</label>
                                 <div class="col-sm-10">
                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password">
                                    <span class="messages"></span>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label class="col-sm-2 col-form-label">Conform Password</label>
                                 <div class="col-sm-10">
                                    <input type="password" class="form-control" id="conform_password" name="conform_password" placeholder="Conform Password">
                                    <span class="messages"></span>
                                 </div>
                              </div>
                              
                              <div class="form-group row">
                                 <label class="col-sm-2"></label>
                                 <div class="col-sm-10">
                                    <input type="submit" name="submit" value="updatepassword">
                                    <!-- <button type="submit" class="btn btn-primary m-b-0">Update Password</button> -->
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
@endsection