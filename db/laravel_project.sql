-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 18, 2018 at 11:55 AM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `materialinward`
--

CREATE TABLE `materialinward` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `GRNNo` varchar(200) NOT NULL,
  `Date` date NOT NULL,
  `SupplierName` varchar(150) NOT NULL,
  `PurchaseNo` varchar(150) NOT NULL,
  `InvoiceNo` varchar(200) NOT NULL,
  `InvoiceDate` date NOT NULL,
  `Status` varchar(150) NOT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `TotalAmount` double(10,2) NOT NULL,
  `TotalSalesTaxAmount` double(10,2) NOT NULL,
  `NetAmount` double(10,2) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `materialinwardlist`
--

CREATE TABLE `materialinwardlist` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `GRNNo` varchar(200) NOT NULL,
  `Product` varchar(150) NOT NULL,
  `Quantity` decimal(10,0) NOT NULL,
  `Price` double(10,2) NOT NULL,
  `Amount` double(10,2) NOT NULL,
  `SalestaxAmount` double(10,2) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mstrbf`
--

CREATE TABLE `mstrbf` (
  `UniqueId` int(11) NOT NULL,
  `BFName` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrcolor`
--

CREATE TABLE `mstrcolor` (
  `UniqueId` int(11) NOT NULL,
  `Color` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrcustomer`
--

CREATE TABLE `mstrcustomer` (
  `UniqueId` int(11) NOT NULL,
  `CustomerCode` varchar(150) NOT NULL,
  `CustomerName` varchar(300) NOT NULL,
  `Address` varchar(1500) NOT NULL,
  `PinCode` decimal(10,0) NOT NULL,
  `MobileNo` varchar(100) NOT NULL,
  `EmailId` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `ContactNo` decimal(10,0) NOT NULL,
  `ContactEmailId` varchar(150) NOT NULL,
  `ContactPersonName` varchar(300) NOT NULL,
  `BranchCode` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrcustomer`
--

INSERT INTO `mstrcustomer` (`UniqueId`, `CustomerCode`, `CustomerName`, `Address`, `PinCode`, `MobileNo`, `EmailId`, `Status`, `ContactNo`, `ContactEmailId`, `ContactPersonName`, `BranchCode`) VALUES
(1, '3434', 'dfdfd', 'kurumberi', '3434', '1234567890', 'guna@gmail.com', 'very good', '43434', 'welcome@gmail.com', 'guna', '343423'),
(2, '2323', 'thala', 'vellore', '45454', '1234567890', 'madhan@gmail.com', 'very good', '987654321', 'welcome@gmail.com', 'sankar', '343423'),
(3, '4545', 'vijya', 'chennai', '652635', '123987654', 'vijya@gmail.com', 'very good', '987654321', 'thala@gmail.com', 'vijya', '8989'),
(4, '2323', 'kavi', 'kurumberi', '232', '1234567890', 'guna@gmail.com', 'very good', '987654321', 'welcome@gmail.com', 'sankar', '343423'),
(5, '6475', 'kala', 'jagan', '32523', '1234567890', 'madhan@gmail.com', 'good', '6533478012', 'thala@gmail.com', 'guna', '343423'),
(6, '8989', 'suresh', 'krishnagiri', '645643', '9087654321', 'suresh@gimail.com', 'very good', '74742721', 'welcome@gmail.com', 'gmltech', '7878'),
(7, '342343', 'kavi', 'kurumberi', '3434', '90875654321', 'guna@gmail.com', 'connection', '987654321', 'welcome@gmail.com', 'sankar', '343423');

-- --------------------------------------------------------

--
-- Table structure for table `mstrgsm`
--

CREATE TABLE `mstrgsm` (
  `UniqueId` int(11) NOT NULL,
  `GSMName` varchar(300) NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrgsm`
--

INSERT INTO `mstrgsm` (`UniqueId`, `GSMName`, `Status`) VALUES
(1, 'sdfsdf', 'sdsfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `mstrproduct`
--

CREATE TABLE `mstrproduct` (
  `UniqueId` int(11) NOT NULL,
  `Category` varchar(150) NOT NULL,
  `ProductCode` varchar(150) NOT NULL,
  `ProductName` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrsupplier`
--

CREATE TABLE `mstrsupplier` (
  `UniqueId` int(11) NOT NULL,
  `SupplierCode` varchar(150) NOT NULL,
  `SupplierName` varchar(300) NOT NULL,
  `Address` varchar(1500) NOT NULL,
  `PinCode` decimal(10,0) NOT NULL,
  `MobileNo` varchar(100) NOT NULL,
  `EmailId` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `ContactNo` decimal(10,0) NOT NULL,
  `ContactEmailId` varchar(150) NOT NULL,
  `ContactPersonName` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrsupplier`
--

INSERT INTO `mstrsupplier` (`UniqueId`, `SupplierCode`, `SupplierName`, `Address`, `PinCode`, `MobileNo`, `EmailId`, `Status`, `ContactNo`, `ContactEmailId`, `ContactPersonName`) VALUES
(1, '4545', 'kannan', 'kurumberi', '232', '1234567890', 'madhan@gmail.com', 'very good', '987654321', 'welcome@gmail.com', 'guna'),
(2, '4545', 'kannan', 'vellore', '232', '1234567890', 'guna@gmail.com', 'very good', '987654321', 'welcome@gmail.com', 'guna');

-- --------------------------------------------------------

--
-- Table structure for table `mstrterms`
--

CREATE TABLE `mstrterms` (
  `UniqueId` int(11) NOT NULL,
  `TermsName` varchar(500) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrunit`
--

CREATE TABLE `mstrunit` (
  `UniqueId` int(11) NOT NULL,
  `Unit` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstruser`
--

CREATE TABLE `mstruser` (
  `UniqueId` int(11) NOT NULL,
  `UserId` varchar(150) NOT NULL,
  `UserName` varchar(200) NOT NULL,
  `Password` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstruser`
--

INSERT INTO `mstruser` (`UniqueId`, `UserId`, `UserName`, `Password`, `Status`) VALUES
(1, '143', 'welcome', 'password', 'very good'),
(2, '6454', 'guna', 'guna', 'nice'),
(3, '2323', 'sathi', 'sathi', 'very good'),
(4, '2838', 'bala', 'bala', 'welcome'),
(5, '8676', 'suresh', 'password', 'connection'),
(6, '3434', 'guna', 'guna', 'very good');

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder`
--

CREATE TABLE `purchaseorder` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `PoNo` varchar(150) NOT NULL,
  `SupplierName` varchar(200) NOT NULL,
  `QuotationNo` varchar(150) NOT NULL,
  `QuotationDate` varchar(200) NOT NULL,
  `Description` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorderlist`
--

CREATE TABLE `purchaseorderlist` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `PoNo` varchar(150) NOT NULL,
  `product` varchar(150) NOT NULL,
  `Qty` decimal(10,0) NOT NULL,
  `Price` double(10,2) NOT NULL,
  `SalesTaxPercent` double(10,2) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  `BranchCode` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotationrequest`
--

CREATE TABLE `quotationrequest` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `SupplierName` varchar(300) NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  `QuoNo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotationrequestlist`
--

CREATE TABLE `quotationrequestlist` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `QuoNo` varchar(150) NOT NULL,
  `Product` varchar(300) NOT NULL,
  `Quantity` decimal(10,0) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `admin`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$QSejL5udJEpceIaLU5TL2.HyqDOLpnP4jlttAeCKtUineBSuYxqS6', NULL, '2018-10-11 01:17:01', '2018-10-11 08:58:43', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `materialinward`
--
ALTER TABLE `materialinward`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `materialinwardlist`
--
ALTER TABLE `materialinwardlist`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstrbf`
--
ALTER TABLE `mstrbf`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrcolor`
--
ALTER TABLE `mstrcolor`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrcustomer`
--
ALTER TABLE `mstrcustomer`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrgsm`
--
ALTER TABLE `mstrgsm`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrproduct`
--
ALTER TABLE `mstrproduct`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrsupplier`
--
ALTER TABLE `mstrsupplier`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrterms`
--
ALTER TABLE `mstrterms`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstrunit`
--
ALTER TABLE `mstrunit`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `mstruser`
--
ALTER TABLE `mstruser`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `purchaseorder`
--
ALTER TABLE `purchaseorder`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `purchaseorderlist`
--
ALTER TABLE `purchaseorderlist`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `quotationrequest`
--
ALTER TABLE `quotationrequest`
  ADD PRIMARY KEY (`UniqueId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `materialinward`
--
ALTER TABLE `materialinward`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `materialinwardlist`
--
ALTER TABLE `materialinwardlist`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstrbf`
--
ALTER TABLE `mstrbf`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstrcolor`
--
ALTER TABLE `mstrcolor`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstrcustomer`
--
ALTER TABLE `mstrcustomer`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mstrgsm`
--
ALTER TABLE `mstrgsm`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mstrproduct`
--
ALTER TABLE `mstrproduct`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstrsupplier`
--
ALTER TABLE `mstrsupplier`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mstrterms`
--
ALTER TABLE `mstrterms`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstrunit`
--
ALTER TABLE `mstrunit`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstruser`
--
ALTER TABLE `mstruser`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `purchaseorder`
--
ALTER TABLE `purchaseorder`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchaseorderlist`
--
ALTER TABLE `purchaseorderlist`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quotationrequest`
--
ALTER TABLE `quotationrequest`
  MODIFY `UniqueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
