-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 19, 2018 at 04:01 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `package`
--

-- --------------------------------------------------------

--
-- Table structure for table `materialinward`
--

DROP TABLE IF EXISTS `materialinward`;
CREATE TABLE IF NOT EXISTS `materialinward` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `RefNo` decimal(10,0) NOT NULL,
  `GRNNo` varchar(200) NOT NULL,
  `Date` date NOT NULL,
  `SupplierName` varchar(150) NOT NULL,
  `PurchaseNo` varchar(150) NOT NULL,
  `InvoiceNo` varchar(200) NOT NULL,
  `InvoiceDate` date NOT NULL,
  `Status` varchar(150) NOT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `TotalAmount` double(10,2) NOT NULL,
  `TotalSalesTaxAmount` double(10,2) NOT NULL,
  `NetAmount` double(10,2) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `materialinwardlist`
--

DROP TABLE IF EXISTS `materialinwardlist`;
CREATE TABLE IF NOT EXISTS `materialinwardlist` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `RefNo` decimal(10,0) NOT NULL,
  `GRNNo` varchar(200) NOT NULL,
  `Product` varchar(150) NOT NULL,
  `Quantity` decimal(10,0) NOT NULL,
  `Price` double(10,2) NOT NULL,
  `Amount` double(10,2) NOT NULL,
  `SalestaxAmount` double(10,2) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mstrbf`
--

DROP TABLE IF EXISTS `mstrbf`;
CREATE TABLE IF NOT EXISTS `mstrbf` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `BFName` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrcolor`
--

DROP TABLE IF EXISTS `mstrcolor`;
CREATE TABLE IF NOT EXISTS `mstrcolor` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `Color` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrcustomer`
--

DROP TABLE IF EXISTS `mstrcustomer`;
CREATE TABLE IF NOT EXISTS `mstrcustomer` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerCode` varchar(150) NOT NULL,
  `CustomerName` varchar(300) NOT NULL,
  `Address` varchar(1500) NOT NULL,
  `PinCode` decimal(10,0) NOT NULL,
  `MobileNo` varchar(100) NOT NULL,
  `EmailId` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `ContactNo` decimal(10,0) NOT NULL,
  `ContactEmailId` varchar(150) NOT NULL,
  `ContactPersonName` varchar(300) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrcustomer`
--

INSERT INTO `mstrcustomer` (`UniqueId`, `CustomerCode`, `CustomerName`, `Address`, `PinCode`, `MobileNo`, `EmailId`, `Status`, `ContactNo`, `ContactEmailId`, `ContactPersonName`, `BranchCode`) VALUES
(1, '123', 'sabarigir', 'hosur', '653241', '9876543210', 'sabari@gmail.com', 'Y', '9876543210', 'sa@gmail.com', 'giri', '123'),
(2, '12456', 'giri', 'asdsad', '6543210', '9876543210', 'giri@gmail.com', 'N', '9876543210', 's@gmail.com', 'g', '123'),
(3, '584', 'guna', 'hosur', '321456', '9876543210', 'guna@gmail.com', 'Y', '9876543210', 'g@gmail.com', 'guna', '1234456');

-- --------------------------------------------------------

--
-- Table structure for table `mstrgsm`
--

DROP TABLE IF EXISTS `mstrgsm`;
CREATE TABLE IF NOT EXISTS `mstrgsm` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `GSMName` varchar(300) NOT NULL,
  `Status` varchar(50) NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrgsm`
--

INSERT INTO `mstrgsm` (`UniqueId`, `GSMName`, `Status`) VALUES
(1, 'sdfsdf', 'sdsfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `mstrproduct`
--

DROP TABLE IF EXISTS `mstrproduct`;
CREATE TABLE IF NOT EXISTS `mstrproduct` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `Category` varchar(150) NOT NULL,
  `ProductCode` varchar(150) NOT NULL,
  `ProductName` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrsupplier`
--

DROP TABLE IF EXISTS `mstrsupplier`;
CREATE TABLE IF NOT EXISTS `mstrsupplier` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `SupplierCode` varchar(150) NOT NULL,
  `SupplierName` varchar(300) NOT NULL,
  `Address` varchar(1500) NOT NULL,
  `PinCode` decimal(10,0) NOT NULL,
  `MobileNo` varchar(100) NOT NULL,
  `EmailId` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `ContactNo` decimal(10,0) NOT NULL,
  `ContactEmailId` varchar(150) NOT NULL,
  `ContactPersonName` varchar(300) NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrsupplier`
--

INSERT INTO `mstrsupplier` (`UniqueId`, `SupplierCode`, `SupplierName`, `Address`, `PinCode`, `MobileNo`, `EmailId`, `Status`, `ContactNo`, `ContactEmailId`, `ContactPersonName`) VALUES
(1, '4545', 'kannan', 'kurumberi', '232', '1234567890', 'madhan@gmail.com', 'very good', '987654321', 'welcome@gmail.com', 'guna'),
(2, '4545', 'kannan', 'vellore', '232', '1234567890', 'guna@gmail.com', 'very good', '987654321', 'welcome@gmail.com', 'guna');

-- --------------------------------------------------------

--
-- Table structure for table `mstrterms`
--

DROP TABLE IF EXISTS `mstrterms`;
CREATE TABLE IF NOT EXISTS `mstrterms` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `TermsName` varchar(500) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrunit`
--

DROP TABLE IF EXISTS `mstrunit`;
CREATE TABLE IF NOT EXISTS `mstrunit` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `Unit` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstruser`
--

DROP TABLE IF EXISTS `mstruser`;
CREATE TABLE IF NOT EXISTS `mstruser` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(150) NOT NULL,
  `UserName` varchar(200) NOT NULL,
  `Password` varchar(150) NOT NULL,
  `Status` varchar(150) NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstruser`
--

INSERT INTO `mstruser` (`UniqueId`, `UserId`, `UserName`, `Password`, `Status`) VALUES
(1, '143', 'welcome', 'password', 'very good'),
(2, '6454', 'guna', 'guna', 'nice'),
(3, '2323', 'sathi', 'sathi', 'very good'),
(4, '2838', 'bala', 'bala', 'welcome'),
(5, '8676', 'suresh', 'password', 'connection'),
(6, '3434', 'guna', 'guna', 'very good');

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder`
--

DROP TABLE IF EXISTS `purchaseorder`;
CREATE TABLE IF NOT EXISTS `purchaseorder` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `RefNo` decimal(10,0) NOT NULL,
  `PoNo` varchar(150) NOT NULL,
  `SupplierName` varchar(200) NOT NULL,
  `QuotationNo` varchar(150) NOT NULL,
  `QuotationDate` varchar(200) NOT NULL,
  `Description` varchar(300) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorderlist`
--

DROP TABLE IF EXISTS `purchaseorderlist`;
CREATE TABLE IF NOT EXISTS `purchaseorderlist` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `RefNo` decimal(10,0) NOT NULL,
  `PoNo` varchar(150) NOT NULL,
  `product` varchar(150) NOT NULL,
  `Qty` decimal(10,0) NOT NULL,
  `Price` double(10,2) NOT NULL,
  `SalesTaxPercent` double(10,2) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotationrequest`
--

DROP TABLE IF EXISTS `quotationrequest`;
CREATE TABLE IF NOT EXISTS `quotationrequest` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `RefNo` decimal(10,0) NOT NULL,
  `SupplierName` varchar(300) NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL,
  `QuoNo` varchar(150) NOT NULL,
  PRIMARY KEY (`UniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotationrequestlist`
--

DROP TABLE IF EXISTS `quotationrequestlist`;
CREATE TABLE IF NOT EXISTS `quotationrequestlist` (
  `UniqueId` int(11) NOT NULL,
  `RefNo` decimal(10,0) NOT NULL,
  `QuoNo` varchar(150) NOT NULL,
  `Product` varchar(300) NOT NULL,
  `Quantity` decimal(10,0) NOT NULL,
  `Status` varchar(150) NOT NULL,
  `BranchCode` varchar(150) NOT NULL,
  `CreatedBy` varchar(150) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdatedBy` varchar(150) NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BranchCode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreatedBy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `BranchCode`, `CreatedBy`, `CreateDate`, `UpdatedBy`, `UpdateDate`, `role`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$QSejL5udJEpceIaLU5TL2.HyqDOLpnP4jlttAeCKtUineBSuYxqS6', NULL, 'HSR', NULL, NULL, NULL, NULL, '1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
