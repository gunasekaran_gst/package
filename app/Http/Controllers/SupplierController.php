<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use App\Http\Controllers\Validator;
use Validator;
class SupplierController extends Controller
{
    public function index()
    {
        //echo "ok"; die;
        if(Session::has('Username')){
            //

        }else{
            return redirect('')->with('flash_message_error','Please Login First');
        }
        $this->data['add'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrsupplier');
        return view('admin.customer.add_supplier',$this->data);


    }

    //Edit Customer data
    public function edit(Request $request,$UniqueId)
    {


        if($_POST){

            $SupplierCode= $request->SupplierCode;
            $SupplierName = $request->SupplierName;
            $PinCode = $request->PinCode;
            $MobileNo = $request->MobileNo;
            $EmailId= $request->EmailId;
            $Status = $request->has('Status') ?'Y' : 'N';
            $ContactNo = $request->ContactNo;
            $ContactEmailId = $request->ContactEmailId;
            $ContactPersonName = $request->ContactPersonName;
            $BranchCode = $request->BranchCode;
            $Address = $request->Address;


            DB::table('mstrsupplier')
                ->where('UniqueId', $UniqueId)
                ->update(['SupplierCode' => $SupplierCode,'SupplierName' => $SupplierName,'PinCode' => $PinCode,'MobileNo' => $MobileNo,'EmailId' => $EmailId,'Status' => $Status,'ContactNo' => $ContactNo,'ContactEmailId' => $ContactEmailId,'ContactPersonName' => $ContactPersonName,'BranchCode' => $BranchCode,'Address' => $Address]);

            return redirect('/admin/supplier')->with ('message',' Upadeted Successfully ');
        }
        $this->data['edit'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrsupplier');
        $this->data['user'] = DB::select('select * from mstrsupplier where UniqueId = ?',[$UniqueId]);
        return view('admin.customer.add_supplier',$this->data);
    }
    // Delete Customer Data
    public function delete($UniqueId){

        DB::delete('delete from mstrsupplier where  UniqueId= ?',[$UniqueId]);
        return redirect('/admin/supplier')->with ('message',' Deleted Successfully');
    }

    // Insert New Customer Data
    public function insert(Request $request)
    {

        $SupplierCode = $request->SupplierCode;
        $SupplierName = $request->SupplierName;
        $Address = $request->Address;
        $PinCode = $request->PinCode;
        $MobileNo = $request->MobileNo;
        $EmailId= $request->EmailId;
        $Status = $request->has('Status') ?'Y' : 'N';
        $ContactNo = $request->ContactNo;
        $ContactEmailId = $request->ContactEmailId;
        $ContactPersonName = $request->ContactPersonName;
        $BranchCode = $request->BranchCode;



        // var_dump($request->all());die;
        DB::insert('insert into mstrsupplier (SupplierCode, SupplierName, Address, PinCode, MobileNo, EmailId, Status, ContactNo, ContactEmailId, ContactPersonName, BranchCode) values(?,?,?,?,?,?,?,?,?,?,?)', [$SupplierCode, $SupplierName, $Address, $PinCode, $MobileNo, $EmailId, $Status, $ContactNo, $ContactEmailId, $ContactPersonName, $BranchCode]);


        //echo "Record inserted successfully.";
        return redirect()->back()->with('message','Customer Added Successfully');


    }



}