<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Mstrcustomer;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use App\Http\Controllers\Validator;
use Validator;
class CustomerController extends Controller
{
    public function index()
    {
        //echo "ok"; die;
        if(Session::has('Username')){
            //

        }else{
            return redirect('')->with('flash_message_error','Please Login First');
        }
        $this->data['add'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrcustomer');
        return view('admin.customer.add_customer',$this->data);

        
    }

    //Edit Customer data
    public function edit(Request $request,$UniqueId)
    {
       
       
        if($_POST){

              $CustomerCode = $request->CustomerCode;
              $CustomerName = $request->CustomerName;
              $PinCode = $request->PinCode;
              $MobileNo = $request->MobileNo;
              $EmailId= $request->EmailId;
              $Status = $request->has('Status') ?'Y' : 'N';
              $ContactNo = $request->ContactNo;
              $ContactEmailId = $request->ContactEmailId;
              $ContactPersonName = $request->ContactPersonName;
              $BranchCode = $request->BranchCode;
              $Address = $request->Address;


              DB::table('mstrcustomer')
                  ->where('UniqueId', $UniqueId)
                  ->update(['CustomerCode' => $CustomerCode,'CustomerName' => $CustomerName,'PinCode' => $PinCode,'MobileNo' => $MobileNo,'EmailId' => $EmailId,'Status' => $Status,'ContactNo' => $ContactNo,'ContactEmailId' => $ContactEmailId,'ContactPersonName' => $ContactPersonName,'BranchCode' => $BranchCode,'Address' => $Address]);
       
              return redirect('/admin/customer')->with ('message',' Upadeted Successfully ');
            }
        $this->data['edit'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrcustomer');
        $this->data['user'] = DB::select('select * from mstrcustomer where UniqueId = ?',[$UniqueId]);
        return view('admin.customer.add_customer',$this->data);
    }
    // Delete Customer Data
    public function delete($UniqueId){

        DB::delete('delete from mstrcustomer where  UniqueId= ?',[$UniqueId]);
        return redirect('/admin/customer')->with ('message',' Deleted Successfully');
    }

    // Insert New Customer Data
    public function insert(Request $request)
    {
      
        $CustomerCode = $request->CustomerCode;
        $CustomerName = $request->CustomerName;
        $Address = $request->Address;
        $PinCode = $request->PinCode;
        $MobileNo = $request->MobileNo;
        $EmailId= $request->EmailId;
        $Status = $request->has('Status') ?'Y' : 'N';
        $ContactNo = $request->ContactNo;
        $ContactEmailId = $request->ContactEmailId;
        $ContactPersonName = $request->ContactPersonName;
        $BranchCode = $request->BranchCode;


         
        // var_dump($request->all());die;
        DB::insert('insert into mstrcustomer (CustomerCode, CustomerName, Address, PinCode, MobileNo, EmailId, Status, ContactNo, ContactEmailId, ContactPersonName, BranchCode) values(?,?,?,?,?,?,?,?,?,?,?)', [$CustomerCode, $CustomerName, $Address, $PinCode, $MobileNo, $EmailId, $Status, $ContactNo, $ContactEmailId, $ContactPersonName, $BranchCode]);

        
       //echo "Record inserted successfully.";
        return redirect()->back()->with('message','Customer Added Successfully');


    }



}