<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Mstrcustomer;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use App\Http\Controllers\Validator;
use Validator;
class GSMController extends Controller
{
    public function index()
    {
        //echo "ok"; die;
        if(Session::has('Username')){
            //

        }else{
            return redirect('')->with('flash_message_error','Please Login First');
        }
        $this->data['add'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrgsm');
        return view('admin.customer.add_gsm',$this->data);


    }

    //Edit Customer data
    public function edit(Request $request,$UniqueId)
    {


        if($_POST){

            $GSMName = $request->GSMName;
            $Status = $request->Status;


            DB::table('mstrgsm')
                ->where('UniqueId', $UniqueId)
                ->update(['GSMName' => $GSMName,'Status' => $Status]);

            return redirect('/admin/gsm')->with ('message',' Upadeted Successfully ');
        }
        $this->data['edit'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrgsm');
        $this->data['user'] = DB::select('select * from mstrgsm where UniqueId = ?',[$UniqueId]);
        return view('admin.customer.add_gsm',$this->data);
    }
    // Delete Customer Data
    public function delete($UniqueId){

        DB::delete('delete from mstrgsm where  UniqueId= ?',[$UniqueId]);
        return redirect('/admin/gsm')->with ('message',' Deleted Successfully');
    }

    // Insert New Customer Data
    public function insert(Request $request)
    {

        $GSMName = $request->GSMName;
        $Status = $request->Status;


        // var_dump($request->all());die;
        DB::insert('insert into mstrgsm (GSMName, Status) values(?,?)', [$GSMName, $Status]);


        //echo "Record inserted successfully.";
        return redirect()->back()->with('message','GSM Added Successfully');


    }



}