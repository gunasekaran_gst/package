<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Cookie;
use DB;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash; 

class AdminController extends Controller
{
    //
    public function login(Request $request){
    	if($request->isMethod('post')){
    		$data =$request->input();
    		if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
    			// echo "Success"; die;
    			//Session One way 
                 $result = User::where(['email'=>$data['email']])->first();
                 if($result!='')
                  {     
                     Session::put('Username',$result->name);  
                     Session::put('BranchCode',$result->BranchCode);  
                     Session::put('role',$result->role);  

                  }
               return redirect('/dashboard');
              

    		}else{
    			//echo "Failed"; die;
    			return redirect('/admin')->with('flash_message_error','Invalid User Name & Password');
    		}
    	}
    	return view('admin_login');
    }
    
    // After Login View Dashboard
    public function dashboard(){
    	
    	return view('dashboard');
    }
    
    // Setting View
    public function settings(){
        
    	return view('settings');
    }

    // Check Password
    public function checkpassword(Request $request){
    	$data = $request->all();
    	$current_password = $data['current_password'];
    	$check_password = User::where(['role'=>'Admin'])->first();
    	if(Hash::check($current_password,$check_password->password)){
    		echo "true"; die;
    	}else{
    		echo "false"; die;
    	}
    }

    //Update Password
    public function updatePassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $check_password = User::where(['email'=>Auth::user()->email])->first();
            $current_password = $data['current_password'];
            if(Hash::check($current_password,$check_password->password)){
               $password = bcrypt($data['new_password']);
               User::where('id','1')->update(['password'=>$password]);
               return redirect('/admin/settings')->with('flash_message_success','Password Updated Successfully');
            }else{
                return redirect('/admin/settings')->with('flash_message_error','Incorrect Current Password');
               
            }

        }
    }

    //Logout 
    public function logout(){

    	Session::flush();
   	return redirect('')->with('flash_message_success','Logged Out Success');
    }


}
