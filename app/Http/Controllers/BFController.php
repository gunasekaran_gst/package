<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Mstrcustomer;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use App\Http\Controllers\Validator;
use Validator;
class BFController extends Controller
{
    public function index()
    {
        //echo "ok"; die;
        if(Session::has('Username')){
            //

        }else{
            return redirect('')->with('flash_message_error','Please Login First');
        }
        $this->data['add'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrbf');
        return view('admin.customer.add_bf',$this->data);


    }

    //Edit Customer data
    public function edit(Request $request,$UniqueId)
    {


        if($_POST){

            $BFName = $request->BFName;
            $Status = $request->Status;


            DB::table('mstrbf')
                ->where('UniqueId', $UniqueId)
                ->update(['BFName' => $BFName,'Status' => $Status]);

            return redirect('/admin/bf')->with ('message',' Upadeted Successfully ');
        }
        $this->data['edit'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstrbf');
        $this->data['user'] = DB::select('select * from mstrbf where UniqueId = ?',[$UniqueId]);
        return view('admin.customer.add_bf',$this->data);
    }
    // Delete Customer Data
    public function delete($UniqueId){

        DB::delete('delete from mstrbf where  UniqueId= ?',[$UniqueId]);
        return redirect('/admin/bf')->with ('message',' Deleted Successfully');
    }

    // Insert New Customer Data
    public function insert(Request $request)
    {

        $BFName = $request->BFName;
        $Status = $request->Status;


        // var_dump($request->all());die;
        DB::insert('insert into mstrbf (BFName, Status) values(?,?)', [$BFName, $Status]);


        //echo "Record inserted successfully.";
        return redirect()->back()->with('message','BF Added Successfully');


    }



}