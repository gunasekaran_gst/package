<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Mstrcustomer;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use App\Http\Controllers\Validator;
use Validator;
class UserController extends Controller
{
    public function index()
    {
        //echo "ok"; die;
        if(Session::has('Username')){
            //

        }else{
            return redirect('')->with('flash_message_error','Please Login First');
        }
        $this->data['add'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstruser');
        return view('admin.customer.add_user',$this->data);


    }

    //Edit Customer data
    public function edit(Request $request,$UniqueId)
    {


        if($_POST){

            $UserId = $request->UserId;
            $UserName = $request->UserName;
            $Password = $request->Password;
            $Status = $request->Status;

            DB::table('mstruser')
                ->where('UniqueId', $UniqueId)
                ->update(['UserId' => $UserId,'UserName' => $UserName,'Password' => $Password,'Status' => $Status]);

            return redirect('/admin/user')->with ('message',' Upadeted Successfully ');
        }
        $this->data['edit'] = TRUE;
        $this->data['user_list'] = DB::select('select * from mstruser');
        $this->data['user'] = DB::select('select * from mstruser where UniqueId = ?',[$UniqueId]);
        return view('admin.customer.add_user',$this->data);
    }
    // Delete Customer Data
    public function delete($UniqueId){

        DB::delete('delete from mstruser where  UniqueId= ?',[$UniqueId]);
        return redirect('/admin/user')->with ('message',' Deleted Successfully');
    }

    // Insert New Customer Data
    public function insert(Request $request)
    {

        $UserId = $request->UserId;
        $UserName = $request->UserName;
        $Password = $request->Password;
        $Status = $request->Status;


        // var_dump($request->all());die;
        DB::insert('insert into mstruser (UserId, UserName, Password, Status) values(?,?,?,?)', [$UserId, $UserName, $Password, $Status]);


        //echo "Record inserted successfully.";
        return redirect()->back()->with('message','User Added Successfully');


    }



}