<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('admin_login');
});

Route::get('admin/gsm','GSMController@index');
Route::post('admin/add_gsm','GSMController@insert');
Route::get('admin/edit_gsm/{UniqueId}','GSMController@edit');
Route::post('admin/edit_gsm/{UniqueId}','GSMController@edit');
Route::get('admin/delete_gsm/{UniqueId}','GSMController@delete');

Route::get('admin/bf','BFController@index');
Route::post('admin/add_bf','BFController@insert');
Route::get('admin/edit_bf/{UniqueId}','BFController@edit');
Route::post('admin/edit_bf/{UniqueId}','BFController@edit');
Route::get('admin/delete_bf/{UniqueId}','BFController@delete');


Route::get('admin/customer','CustomerController@index');
Route::post('admin/add_customer','CustomerController@insert');
Route::get('admin/edit_customer/{UniqueId}','CustomerController@edit');
Route::post('admin/edit_customer/{UniqueId}','CustomerController@edit');
Route::get('admin/delete_customer/{UniqueId}','CustomerController@delete');

Route::get('admin/supplier','SupplierController@index');
Route::post('admin/add_supplier','SupplierController@insert');
Route::get('admin/edit_supplier/{UniqueId}','SupplierController@edit');
Route::post('admin/edit_supplier/{UniqueId}','SupplierController@edit');
Route::get('admin/delete_supplier/{UniqueId}','SupplierController@delete');


Route::get('admin/user','UserController@index');
Route::post('admin/add_user','UserController@insert');
Route::get('admin/edit_user/{UniqueId}','UserController@edit');
Route::post('admin/edit_user/{UniqueId}','UserController@edit');
Route::get('admin/delete_user/{UniqueId}','UserController@delete');



//Login Texting Process
// Route::post('/login',function(){
// 	echo "work";
// });
// Route::post('/login','LoginController@login');
// Route::get('/home', function () {
//     return view('home');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
//Route::get('/admin','AdminController@login');
Route::match(['get','post'],'/admin','AdminController@login');



//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('dashboard','AdminController@dashboard');
Route::get('settings','AdminController@settings');
//Route::get('/admin/checkpassword','AdminController@checkpassword');
Route::match(['get','post'],'/admin/updatepassword','AdminController@updatePassword');




//with out sesstion, middleware view dashboard
// Route::get('/admin/dashboard','AdminController@dashboard');

Route::get('/logout','AdminController@logout');